package com.example.admin_project

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.firebase.database.DatabaseReference
import com.example.admin_project.R
import com.example.admin_project.SeputarTanam
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.seputartanam_list.view.*


class SeputarTanamAdapter(private val context: Context, private val myref: DatabaseReference, options: FirebaseRecyclerOptions<SeputarTanam>) :
    FirebaseRecyclerAdapter<SeputarTanam, SeputarTanamAdapter.RdbSeputarViewHolder>(options) {

    class RdbSeputarViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        fun bindItem(seputarTanam: SeputarTanam) {
            view.apply {
                val name = "Nama Tanaman  : ${seputarTanam.namaTanaman}"
                val addr = "Manfaat : ${seputarTanam.deskripsi}"
                val gambar = seputarTanam.imageTanam

                tv_name.text = name
                tv_adress.text = addr
                Picasso.with(itemView.context).load(gambar).into(tv_image)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RdbSeputarViewHolder {
        return RdbSeputarViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.seputartanam_list, parent, false))
    }

    override fun onBindViewHolder(holder: RdbSeputarViewHolder, position: Int, seputarTanam: SeputarTanam) {
        holder.bindItem(seputarTanam)
        holder.itemView.setOnClickListener {
            showDialogMenu(seputarTanam)
        }
    }

    private fun showDialogMenu(seputarTanam: SeputarTanam) {
        //dialog popup edit hapus
        val builder = AlertDialog.Builder(context, R.style.ThemeOverlay_MaterialComponents_Dialog_Alert)
        val option = arrayOf("Edit", "Hapus")
        builder.setItems(option) { dialog, which ->
            when (which) {
                0 -> context.startActivity(Intent(context, AddEditActivity::class.java).apply {
                    putExtra(AddEditActivity.REQ_EDIT, true)
                    putExtra(AddEditActivity.EXTRA_DATA, seputarTanam)
                })
                1 -> showDialogDel(seputarTanam)
            }
        }

        builder.create().show()
    }

    private fun showDialogDel(seputarTanam: SeputarTanam) {
        val builder = AlertDialog.Builder(context, R.style.ThemeOverlay_MaterialComponents_Dialog_Alert)
            .setTitle("Hapus Data ?")
            .setMessage("Yakin Mau Hapus ${seputarTanam.namaTanaman} ?")
            .setPositiveButton(android.R.string.yes) { dialog, which ->
                deleteById(seputarTanam.id_tanam)
            }.setNegativeButton(android.R.string.cancel, null)
        builder.create().show()
    }

    private fun deleteById(id_tanam: String) {
        myref.child(id_tanam).removeValue().addOnCompleteListener { task ->
            if (task.isSuccessful) {
                Toast.makeText(context, "Success Delete data", Toast.LENGTH_SHORT).show()
            } else {
                Toast.makeText(context, "Failute Delete data", Toast.LENGTH_SHORT).show()
            }
        }
    }
}