package com.example.admin_project

import android.os.Parcelable
import android.widget.ImageView
import kotlinx.android.parcel.Parcelize
@Parcelize
data class SeputarTanam(
    var id_tanam: String = "",
    var namaTanaman: String? = null,
    var deskripsi: String? = null,
    var imageTanam: String? = null
) : Parcelable