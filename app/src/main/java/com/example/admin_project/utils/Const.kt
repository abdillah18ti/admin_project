package com.example.admin_project.utils

import android.content.Context
import android.content.Intent
import android.os.Bundle

object Const {
    val PATH_COLLECTION = "SeputarTanam"
    val PATH_NAME = "namaTanaman"

    fun setTimeStamp(): Long {
        val time = (-1 * System.currentTimeMillis())
        return time
    }
}

fun <T> Context.openActivity(it: Class<T>, extras: Bundle.() -> Unit = {}) {
    val intent = Intent(this, it)
    intent.putExtras(Bundle().apply(extras))
    startActivity(intent)
}
