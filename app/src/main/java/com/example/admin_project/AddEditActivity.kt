package com.example.admin_project

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.text.Editable
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.admin_project.utils.Const
import com.google.android.gms.tasks.Continuation
import com.google.android.gms.tasks.Task
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import kotlinx.android.synthetic.main.activity_add_edit.*
import kotlinx.android.synthetic.main.seputartanam_list.*
import java.io.IOException
import java.util.*
import kotlin.collections.HashMap



class AddEditActivity : AppCompatActivity (){
    companion object {
        //key untuk intent data
        const val EXTRA_DATA = "extra_data"
        const val REQ_EDIT = "req_edit"
    }

    private var isEdit = false
    private var tanaman: SeputarTanam? = null

    private val mFirebaseDatabase = FirebaseDatabase.getInstance()
    private val myref = mFirebaseDatabase.getReference(Const.PATH_COLLECTION)

    private val PICK_IMAGE_REQUEST = 71
    private var filePath: Uri? = null

    private var firebaseStore: FirebaseStorage? = null
    private var storageReference: StorageReference? = null

    private lateinit var imageView: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_edit)

        isEdit = intent.getBooleanExtra(REQ_EDIT, false)
        tanaman = intent.getParcelableExtra(EXTRA_DATA)

        firebaseStore = FirebaseStorage.getInstance()
        storageReference = FirebaseStorage.getInstance().reference

        btn_browse.setOnClickListener { launchGallery() }
        btn_save.setOnClickListener {
            saveData()
            uploadImage()
        }
        initView()
    }

    private fun launchGallery() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST)
    }

    private fun initView() {
        if (isEdit) {
            btn_save.text = getString(R.string.update)
            ti_name.text = Editable.Factory.getInstance().newEditable(tanaman?.namaTanaman)
            ti_address.text = Editable.Factory.getInstance().newEditable(tanaman?.deskripsi)
        }
    }

    private fun saveData() {
        setData(tanaman?.id_tanam)
    }

    private fun setData(strId: String?) {
        createSeputar(strId)
    }

    private fun createSeputar(id_tanam: String?) {
        val name = ti_name.text.toString().trim()
        val desc = ti_address.text.toString().trim()

        val refe = storageReference?.child("uploads/" + UUID.randomUUID().toString())
        val uploadTask = refe?.putFile(filePath!!)

        val urlTask = uploadTask?.continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task ->
            if (!task.isSuccessful) {
                task.exception?.let {
                    throw it
                }
            }
            return@Continuation refe.downloadUrl
        })?.addOnCompleteListener { task ->
            if (task.isSuccessful) {

                val downloadUri = task.result
                val imgurl = downloadUri.toString()


                if (name.isEmpty() or desc.isEmpty() or imgurl.isEmpty()) {
                    Toast.makeText(
                        this, "Isi data secara lengkap tidak boleh kosong",
                        Toast.LENGTH_SHORT
                    )
                        .show()
                    return@addOnCompleteListener
                }

                val rsId = myref.push().key
                val rs = SeputarTanam(rsId!!, name, desc, imgurl)

                myref.child(rsId).setValue(rs).addOnCompleteListener {
                    Toast.makeText(
                        applicationContext, "Data berhasil ditambahkan",
                        Toast.LENGTH_SHORT
                    )
                        .show()
                }

            } else {
                // Handle failures
            }
        }?.addOnFailureListener{

        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK) {
            if(data == null || data.data == null){
                return
            }

            filePath = data.data
            try {
                val bitmap = MediaStore.Images.Media.getBitmap(contentResolver, filePath)
                imageView = findViewById(R.id.previewImage)
                imageView.setImageBitmap(bitmap)
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }
    private fun uploadImage(){
        if(filePath != null){
            val ref = storageReference?.child("uploads/" + UUID.randomUUID().toString())
            val uploadTask = ref?.putFile(filePath!!)

            val urlTask = uploadTask?.continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task ->
                if (!task.isSuccessful) {
                    task.exception?.let {
                        throw it
                    }
                }
                return@Continuation ref.downloadUrl
            })?.addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val downloadUri = task.result
                    addUploadRecordToDb(downloadUri.toString())
                } else {
                     //Handle failures
                }
            }?.addOnFailureListener{

            }
        }else{
            Toast.makeText(this, "Please Upload an Image", Toast.LENGTH_SHORT).show()
        }
    }
    private fun addUploadRecordToDb(uri: String){
        val db = FirebaseFirestore.getInstance()

        val data = HashMap<String, Any>()
        data["imageUrl"] = uri

        db.collection("posts")
            .add(data)
            .addOnSuccessListener { documentReference ->
                Toast.makeText(this, "Saved to DB", Toast.LENGTH_LONG).show()
            }
            .addOnFailureListener { e ->
                Toast.makeText(this, "Error saving to DB", Toast.LENGTH_LONG).show()
            }
    }
}
