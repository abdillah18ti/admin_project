package com.example.admin_project

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.firebase.database.FirebaseDatabase
import com.example.admin_project.R
import com.example.admin_project.SeputarTanam
import com.example.admin_project.utils.Const
import com.example.admin_project.utils.Const.PATH_NAME
import kotlinx.android.synthetic.main.activity_rdb_seputartanam.*


class SeputarTanamActivity : AppCompatActivity(){
    private val mFirebase = FirebaseDatabase.getInstance()
    private val myref = mFirebase.getReference(Const.PATH_COLLECTION)
    private val query = myref.orderByChild(PATH_NAME)

    private lateinit var adapter: FirebaseRecyclerAdapter<SeputarTanam, SeputarTanamAdapter.RdbSeputarViewHolder>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rdb_seputartanam)
        initView()
        setupAdapter()

        fab_firedb.setOnClickListener {
            startActivity(Intent(this, AddEditActivity::class.java).apply {
                putExtra(AddEditActivity.REQ_EDIT, false)
            })
        }
    }

    private fun initView() {
        supportActionBar?.title = "Simple CRUD Realtime Database"
        rv_firedb.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(this@SeputarTanamActivity, LinearLayoutManager.VERTICAL, false)
        }
    }

    private fun setupAdapter() {
        val option = FirebaseRecyclerOptions.Builder<SeputarTanam>()
            .setQuery(query, SeputarTanam::class.java)
            .build()

        adapter = SeputarTanamAdapter(this, myref, option)
        adapter.notifyDataSetChanged()
        rv_firedb.adapter = adapter
    }

    override fun onStart() {
        adapter.startListening()
        super.onStart()
    }

    override fun onStop() {
        adapter.stopListening()
        super.onStop()
    }
}