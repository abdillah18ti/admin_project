package com.example.admin_project

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_login.*

class Login : AppCompatActivity() {
    private var inputusername: EditText? = null
    private var inputpassword: EditText? = null
    private var button_submit: Button? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        inputusername = findViewById(R.id.username)
        inputpassword = findViewById(R.id.password)
        button_submit = findViewById(R.id.btn_submit)

        btn_submit.setOnClickListener(View.OnClickListener {
            val inputusername = username.getText().toString().trim { it <= ' ' }
            val inputpassword = password.getText().toString().trim { it <= ' ' }
            if (inputusername.isEmpty()) {
                Toast.makeText(
                    applicationContext,
                    "Enter Email Address",
                    Toast.LENGTH_SHORT
                ).show()
            } else if (inputpassword.isEmpty()) {
                Toast.makeText(applicationContext, "Enter Password", Toast.LENGTH_SHORT)
                    .show()
            } else if (inputusername == "admin" && inputpassword == "admin") {
                Toast.makeText(applicationContext, "Login Successful", Toast.LENGTH_SHORT)
                    .show()
                val intent = Intent(applicationContext, TampilanAwalAdmin::class.java)
                startActivity(intent)
            } else {
                Toast.makeText(
                    applicationContext,
                    "Input Username",
                    Toast.LENGTH_SHORT
                ).show()
            }
        })
    }
}